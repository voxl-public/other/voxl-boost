# voxl-boost

This repo contains build scripts to build the full Boost library for VOXL platforms. MPI and Python libraries are not built as they seem to not be compatible with VOXL at this time. 

Note: Due to the old compiler version (GCC-4.9) use when building this library, an older Boost version (1.65.0) is used.


## Build Instructions

1) prerequisite: voxl-cross docker image >= V1.1

Follow the instructions here:

https://gitlab.com/voxl-public/voxl-docker


2) Launch Docker and make sure this project directory is mounted inside the Docker.

4) Compile the boost library
```bash
./build.sh
```

5) Make an ipk package inside the docker.

```bash
voxl-cross:~$  ./make_ipk.sh

Package Name:  libboost_arm64
version Number:  x.x.x
ar: creating libboost_arm64_x.x.x.ipk

DONE
```

This will make a new libboost_arm64_x.x.x.ipk file in your working directory. The name and version number came from the ipk/control/control file. If you are updating the package version, edit it there.


## Deploy to VOXL

Make sure VOXL has system image 3.3.0 or later.

You can now push the ipk package to the VOXL and install with opkg however you like. To do this over ADB, you may use the included helper script: install_on_voxl.sh.

Do this OUTSIDE of docker as your docker image probably doesn't have usb permissions for ADB.

```bash
~/git/voxl-dfs-server$ ./install_on_voxl.sh
pushing libboost_arm64_x.x.x.ipk to target
searching for ADB device
adb device found
libboost_arm64_x.x.x.ipk: 1 file pushed. XXXX MB/s (XXXXX bytes in XXXXXs)
Removing package libboost_arm64 from root...
Installing libboost_arm64 (x.x.x) on root.
Configuring libboost_arm64

Done installing libboost_arm64
```

